#!/usr/bin/env python3

"""
how to parse /proc/stat - https://man7.org/linux/man-pages/man5/proc.5.html
wtf is 136 char device - https://www.kernel.org/doc/Documentation/admin-guide/devices.txt
"""


from typing import Tuple
from collections import namedtuple
import pathlib
import os

PROC_DIR = pathlib.Path("/proc")
CHAR_DEVICES_DIR = pathlib.Path("/dev/char")


def get_tty(tty_nr: int) -> str:
    """convert tty_nr to human readable format"""

    if tty_nr == 0:
        return "?"
    major, minor = os.major(tty_nr), os.minor(tty_nr)
    if 136 <= major <= 143:
        # Unix98 PTY slave
        return f"pts/{minor}"
    # symlink to tty device
    device_path = (CHAR_DEVICES_DIR / f"{major}:{minor}").resolve()
    return device_path.name


def ticks_to_str(ticks: int, rate: int):
    """convert cpu ticks into human readable format"""
    seconds = ticks // rate
    hours = seconds // 3600
    minutes = seconds % 3600 // 60
    seconds = seconds % 60 
    if hours:
        return f"{hours:02}:{minutes:02}:{seconds:02}"
    return f"{minutes:02}:{seconds:02}"


def parse_stat_string(stat_string: str) -> Tuple[str]:
    """naive /proc/${pid}/stat parser implementation"""
    parenthesis = False
    element = ""
    for ch in stat_string:
        if ch == "(":
            parenthesis = True
        elif ch == ")":
            parenthesis = False        
        elif ch != " " or parenthesis:
            element += ch
        else:
            yield element
            element = ""
    else:
        yield element


ProcStat = namedtuple("ProcStat", ["pid", "tty", "state", "cpu_time"])


def get_stat(proc_dir: pathlib.Path, rate: int):
    """read stat file"""
    (
        pid, _comm, state, _ppid, _pgrp, _session, tty_nr, _tpgid,
        _flags, _minflt, _cminflt, _majflt, _cmajflt,
        utime, stime, *_,
    ) = parse_stat_string((proc_dir / "stat").read_text().strip())
    return ProcStat(
        pid=int(pid),
        tty=get_tty(int(tty_nr)),
        state=state,
        cpu_time=ticks_to_str(int(utime) + int(stime), rate),
        )


def get_cmd(proc_path: pathlib.Path):
    cmd = (proc_path / "cmdline").read_bytes().replace(b"\0", b" ").decode()
    if not cmd:
        cmd = f"[{(proc_path / 'comm').read_text().strip()}]"
    return cmd


def main() -> int:
    ticks_per_second = os.sysconf("SC_CLK_TCK")
    w, _ = os.get_terminal_size(0)
    template = "{:>7} {:5s} {:5s} {:10s} {}"
    print(template.format("PID", "TTY", "STAT", "TIME", "COMMAND"))
    for subdir in PROC_DIR.iterdir():
        pid = subdir.name
        if not pid.isnumeric():
            continue
        
        cmd = get_cmd(subdir)
        stat = get_stat(subdir, ticks_per_second)
        line = template.format(*stat, cmd)
        print(line[:w])

        
if __name__ == "__main__":
    exit(main())
